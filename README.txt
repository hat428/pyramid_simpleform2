pyramid_simpleform README
=========================

This is fork (likely temporary) of the python package:

  `pyramid_simpleform <https://github.com/Pylons/pyramid_simpleform>`_

I am working to update the code to work with python 3.4,
including using the newer webhelpers2 package instead of the original
(and not ready for python3) webhelpers.

Warning: The module package created by this code is still called
pyramid_simpleform and therefor will clash with the original, so you
can't have them both installed in the same python environment (but why
would you want to do that?).
 
This **pyramid_simpleform2**, like the original, offers simple form
utilities for using `pyramid <http://www.pylonsproject.org/>`_ with
`formencode <https://github.com/formencode/formencode>`_ and
`webhelpers2 <https://github.com/mikeorr/WebHelpers2>`_.

It should still work pretty much as the original documentation states,
which can be found at http://packages.python.org/pyramid_simpleform/

This new version here however is untested and NOT production ready.